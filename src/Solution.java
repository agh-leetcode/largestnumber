import java.util.Arrays;
import java.util.Comparator;

public class Solution {
    public static void main(String[] args) {
        long start  = System.currentTimeMillis();
        Solution solution = new Solution();
        System.out.println("     Without lambda");
        System.out.println("result: " + solution.largestNumber1(new int[]{10, 2, 0, 943, 72, 23, 12, 5,3, 54}));
        System.out.println("result: " + solution.largestNumber1(new int[]{20, 23, 0, 23, 702, 7, 122, 12,3, 4}));
        System.out.println("result: " + solution.largestNumber1(new int[]{224, 43, 98, 3, 12, 2, 1290, 15,11, 22}));
        System.out.println("time: " + (System.currentTimeMillis()-start) + " ms");

        long secondStart = System.currentTimeMillis();
        System.out.println("\n     With lambda");
        System.out.println("result: " + solution.largestNumber2(new int[]{10, 2, 0, 943, 72, 23, 12, 5,3, 54}));
        System.out.println("result: " + solution.largestNumber2(new int[]{20, 23, 0, 23, 702, 7, 122, 12,3, 4}));
        System.out.println("result: " + solution.largestNumber2(new int[]{224, 43, 98, 3, 12, 2, 1290, 15,11, 22}));
        System.out.println("time: " + (System.currentTimeMillis()-secondStart) + " ms");
    }

    //without lambda
    public String largestNumber1(int[] nums) {
        if (nums.length==0)
            return "";
        String [] numbers = new String[nums.length];

        for (int i = 0; i < nums.length; i++) {
            numbers[i]= String.valueOf(nums[i]);
        }

        Arrays.sort(numbers, new Comparator<String>(){
            public int compare(String a, String b){
                String s1 = a+b;
                String s2 = b+a;
                return s2.compareTo(s1);
            }
        });

        if(numbers[0].equals("0"))
            return "0";


        StringBuilder result = new StringBuilder();

        for (String s : numbers) {
            result.append(s);
        }

        return result.toString();
    }

    //with lambda
    public String largestNumber2(int[] nums) {
        if (nums.length==0)
            return "";
        String [] numbers = new String[nums.length];

        for (int i = 0; i < nums.length; i++) {
            numbers[i]= String.valueOf(nums[i]);
        }

        Comparator<String> comparator = (s1, s2) -> (s2+s1).compareTo(s1+s2);
        Arrays.sort(numbers,comparator);


        if(numbers[0].equals("0"))
            return "0";


        StringBuilder result = new StringBuilder();

        for (String s : numbers) {
            result.append(s);
        }

        return result.toString();
    }
}
